package org.droidtr.stocktakip;

import android.graphics.drawable.*;
import static org.droidtr.stocktakip.ColorUtils.*;

public class DrawableUtils {
	
	public static final Drawable getButtonBackground(){
		return getBackground(TEAL,TEAL_LIGHT);
	}
	
	public static final Drawable getBackground(int color1,int color2){
		StateListDrawable sld = new StateListDrawable();
		GradientDrawable gd = new GradientDrawable();
		gd.setColor(color1);
		gd.setCornerRadius(DensityUtils.dpInt(8));
		sld.addState(new int[]{android.R.attr.state_pressed},gd);
		GradientDrawable nd = new GradientDrawable();
		nd.setColor(color2);
		nd.setCornerRadius(DensityUtils.dpInt(8));
		sld.addState(new int[]{},nd);
		return sld;
	}
	
}
