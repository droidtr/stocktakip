package org.droidtr.stocktakip;

public class ColorUtils{
//normal renkler
	public static final int RED=0xFFFF0000;
	public static final int GREEN=0xFF00FF00;
	public static final int BLUE=0xFF0000FF;
	public static final int BLACK=0xFF000000;
	public static final int WHITE=0xFFFFFFFF;
	public static final int MAGENTA=0xFFFF00FF;
	public static final int YELLOW=0xFFFFFF00;
	public static final int CYAN=0xFF00FFFF;
	//material renkler
	public static final int TEAL=0xFF008080;
	public static final int TEAL_LIGHT=0xFF009090;
}
