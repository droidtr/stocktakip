package org.droidtr.stocktakip;

import android.app.Activity;
import android.os.Bundle;
import android.os.Build;
import android.view.*;
import android.graphics.*;
import android.view.View.*;
import android.widget.*;
import android.content.*;
import static org.droidtr.stocktakip.DensityUtils.*;
import static org.droidtr.stocktakip.DrawableUtils.*;
import static org.droidtr.stocktakip.LayoutUtils.*;
import static org.droidtr.stocktakip.ColorUtils.*;

public class MainActivity extends Activity {
	public bmpdb bmpdb=new bmpdb();
	Activity c=null;
	private LinearLayout mainLayout=null;
	private LinearLayout h1,h2,h3,v1;
	public void onCreate(Bundle instance){
		c=this;
		super.onCreate(instance);
		mainLayout = VBox(c,false);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		mainLayout.addView(ActionBar(c));
		setContentView(mainLayout);
		getWindow().setStatusBarColor(TEAL);
		getWindow().setNavigationBarColor(TEAL);
		h1=HBox(this);
		h2=HBox(this);
		h3=HBox(this);
		v1=VBox(this);
		h1.addView(getButton("Ürün gör",0));
		h1.addView(getButton("Ürün ekle",0));
		h2.addView(getButton("Ürün güncelle",0));
		h2.addView(getButton("Ürün transferi",0));
		h3.addView(getButton("Ürün miktar ayarlama",0));
		v1.addView(getEntry(this,"Ürün numarası"));
		v1.addView(h1);
		v1.addView(h2);
		v1.addView(h3);
		mainLayout.addView(v1);
	}
	public Button getButton(String label,int num){
		return LayoutUtils.getButton(this,label,ocl(num));
	}
	public OnClickListener ocl(final int num){
		return new OnClickListener(){
			public void onClick(View p1){
				if(num==0){ //ilk ekran
					
				}else if(num==1){ //ikinci ekran
					
				}
			}
		};
	}
}
